# SHEEPS STUDIOS WEBSITE : README

### INSTALLATION
Pour installer l'environnement sur vos machines assurez vous d'avoir :
* [...]  Node js installé https://nodejs.org/en/ *assurez vous de prendre **la version LTS** long term support qui aura tendance à être plus stable*
Pour vérifier l'installation de node js faites dans un terminal la commande **node --version**
* [...] Récupérez l'url du projet dans git https://gitlab.com/sheep-olata/sheepolata_website puis effectuez un git clone du projet dans un emplacement définitif que vous ne troucherez plus.
    * git clone https://gitlab.com/sheep-olata/sheepolata_website.git
* [...] Installez l'outil Git Fork https://git-fork.com/ qui permet de gérer les intéractions git de manière visuelle ce qui limitera les erreurs.
    * Dans file/open repository sélectionnez l'emplacement du projet que vous avez cloné ou est situé le fichier .git (pour l'afficher dans le gestionnaire de fichiers aller dans affichage/éléments masqués)
* [...] Ouvrez le projet dans Visual Studio placez vous ou est situé le fichier server.js qui sera le fichier hébergeant le serveur.  
Effectuez un **npm install** pour re télécharger l'ensemble des dépendances.
* [...] Lancez le projet en faisant **node server.js** pour le lancer en mode de fonctionnement basique ou **npm run persist** pour utiliser l'extension nodemon.
* [...] Connectez-vous à **localhost:port** sur votre navigateur pour vous connecter au site.

### CONFIGURATION
Lors de l'initialisation du projet des modifications ont été apportées au projet.

* Dans package.json : Ajout dans les scripts de la fonctionnalité persist qui permet l'utilisation de nodemon.
A titre d'information les versions affichées ^1.19.0 permettent l'évolution automatique vers la version 1.20.0 lors de sa sortie.
Les versions ~1.19.0 permettent l'évolution automatique vers la version 1.19.1 lors de sa sortie mais pas de son passage à la 1.20.0 car jugé sensible.
* Dans visual studio dans file/preferences/settings recherche du parametre javascript.validate.enable pour décocher sa case (permet l'utilisation de jshint)
* Dans l'arborescence du projet création d'un fichier de configuration .gitignore https://www.toptal.com/developers/gitignore (à faire évoluer au fil du temps)
* Dans l'arborescence du projet création d'un fichier de configuration .jshintrc qui permet l'utilisation de la version es6 et désactivant les retours d'erreur sur les const.
* Installation des modules complémentaires Visual Studio EJS et Jshint (en tant qu'administrateur)


### DEPENDANCIES
Toutes les dépendances du projet peuvent être retrouvées dans le fichier ```package.json``` : 
Dépendance | Description | Installation
------------ | ------------- | -------------
```body-parser``` | Body parser est un middleware (intergiciel) qui permet de parser les requêtes reçues *req.body.foo.toString()* | `npm install --save body-parser`
```ejs``` | Ejs est un moteur de template qui nous permet de découper nos pages html en pages ou en composants réutilisables à l'aide de balises simples. | `npm install --save ejs`
```express``` | Express est une librairie permet la création et l'utilisation d'un serveur web facilement et rapidement | `npm install --save express`
```jshint``` | Js hint est un linter javascript qui permet la mise en évidence d'erreurs de syntaxe | `npm install --save jshint`
```nodemon``` | Nodemon est une librairie qui permet le relancement automatique du serveur après que l'on ait effectué des modifications puis sauvegardé. | `npm install --save nodemon`
```socket.io``` | Socket.io facilite la communication bidirectionnelle entre client et serveur. https://stackoverflow.com/questions/17696801/express-js-app-listen-vs-server-listen | `npm install --save socket.io`
