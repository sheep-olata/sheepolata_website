// Make the scrolling action smooth
function initScroll() {
    // When the page is loaded
    $(document).ready(function() {
        // If click on script-scroll object
        $('.script-scroll').on('click', function() {
            // Go to the target page
            let SCROLL_PAGE = $(this).attr('href');
            // Scrolling speed (ms) 
            let SCROLL_SPEED = 1200;
            let SCROLL_OFFSET; //62
            // Moves slower at the beginning/end, faster in the middle
            let SCROLL_EASING = "swing";

            // Prevent home offset
            if(SCROLL_PAGE == "#home"){ SCROLL_OFFSET = 70; }
            else { SCROLL_OFFSET = -10; }
        
            // Launch the scrolling animation 
            $('html, body').animate( { scrollTop: $(SCROLL_PAGE).offset().top - SCROLL_OFFSET}, SCROLL_SPEED, SCROLL_EASING );
            return false;
        });
    });
}


function initAnimations() {
    let $animated = $(".animated");

    $animated.appear({
        force_process: true
    });

    $animated.on("appear", function() {

        let $el = $(this);

        let animation = $el.data("animation");
        let delay     = $el.data("delay");

        // If there is a delay play the animation after waiting
        // Else play the animation directly
        if (delay) {

            setTimeout(function() {
                $el.addClass(animation);
                $el.addClass("showing");
                $el.removeClass("hiding");
            }, delay);
        } else {
            $el.addClass(animation);
            $el.addClass("showing");
            $el.removeClass("hiding");
        }
    });
}

// Initialize functions when the document is loaded
$(document).ready(function() {
    initScroll();
    initAnimations();
});

// Initialize loading classes when the window is loading
$(window).on("load", function() {

    let $loader = $(".loader");

    $loader.find(".loading").fadeOut();
    $loader.fadeOut("slow");
});


function hideOtherProducts(product) {
    // Get products
    PRODUCT_1 = document.getElementById("product-1");
    PRODUCT_2 = document.getElementById("product-2");
    PRODUCT_3 = document.getElementById("product-3");
    // Get reset component
    PRODUCTS_RESET = document.getElementById("products-reset");
    // Get other components
    // PRODUCTS_MORE = document.getElementById("products-more");
    // Add products into an array 
    let products = [PRODUCT_1, PRODUCT_2, PRODUCT_3];

    // If the reset cross is selected display all products and hide it
    if(product == 0) {
        //  Browse the list of products to display all of them
        for(i=1; i<(products.length+1); i++) {
            products[i-1].style.display = "block";
        }
        // At the end of the loop hide the cross
        PRODUCTS_RESET.style.display = "none";
        // Add the button more
        // PRODUCTS_MORE.style.display = "initial";
    }
    else {
        // Browse the list of products starting at 1
        for(i=1; i<(products.length+1); i++) {
            // If product = selected product we display it, add reset cross and hide more products button
            // Else we hide them
            if(i == product) {
                         PRODUCTS_RESET.style.display = "block";
                        //  PRODUCTS_MORE.style.display  = "none";
                products[i-1].style.display           = "block";
            }
            else { 
                products[i-1].style.display = "none";
            }
        }
    }
}

function closeModal() {
    CLOSE               = document.getElementById('connection-modal');
    CLOSE.style.display = 'none';
}

function connect(view) {
    let LOGIN = document.getElementById('login-container');
    let REGISTER   = document.getElementById('register-container');
    let FORGOT     = document.getElementById('forgot-container');

    switch(view) {
        case 1:
            LOGIN.style.display = 'block';
            REGISTER.style.display = 'none';
            FORGOT.style.display = 'none';
            break;
        case 2:
            LOGIN.style.display = 'none';
            REGISTER.style.display = 'block';
            FORGOT.style.display = 'none';
            break;
        case 3:
            LOGIN.style.display = 'none';
            REGISTER.style.display = 'none';
            FORGOT.style.display = 'block';
            break;
        default:
            LOGIN.style.display = 'block';
            REGISTER.style.display = 'none';
            FORGOT.style.display = 'none';
    }
}