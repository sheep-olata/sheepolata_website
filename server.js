// Use express to create the server
const express = require('express');
const app     = express();
// Use http to run a http server
const server   = require('http').createServer(app);
const socketIO = require('socket.io').listen(server);
// Use express sessions for authentication
const session  = require('express-session');
const passport = require('passport');
// Use express layout to manage ejs files
const expressLayouts = require('express-ejs-layouts');
// Use mongoose to connect the database
const mongoose = require('mongoose');
// Use additional libraries 
const flash      = require('connect-flash');
const bodyParser = require('body-parser');
const path       = require('path');
// Configuration files
const db      = require('./config/database').mongoURI;
const display = require('./config/colors');
const env     = require('./config/environment');
require('./config/passport')(passport);

// Application port
const PORT = 4200;



// SocketIO pool
// users       = [];
// connections = [];


// Connect to MongoDB
mongoose.connect(db,{ useNewUrlParser: true ,useUnifiedTopology: true})
        .then(() => console.log(display.important, "[+] Listening database on port 27017", display.reset))
        .catch(err => console.log(err));


// Use EJS template engine
app.use(expressLayouts);
app.set('view engine', 'ejs');

// Use body parser to parse requests
app.use(express.urlencoded({ extended: false }));

// Use Express session properties
app.use(session(env.SESSION));

// Use passport session
app.use(passport.initialize());
app.use(passport.session());


// Connect flash
// Used to run express layouts
app.use(flash());

// Global variables
app.use(function(req, res, next) {
    res.locals.success_msg = req.flash('success_msg');
    res.locals.error_msg   = req.flash('error_msg');
    res.locals.error       = req.flash('error');
    next();
});

  
// Set Public folder /!\ Don't forget the first '/'
app.use('/assets', express.static(path.join(__dirname, 'public')));


// Set Routes
app.use('/', require('./routes/usersRoute.js')); 
app.use('/', require('./routes/index.js'));
app.use('*', require('./routes/errors.js'));





//Listen at sockets
/*socketIO.sockets.on('connection', function(socket) {
    // Connect function
    connections.push(socket);
    console.log(display.info, '[i] connected:', connections.length , display.info, 'sockets connected', display.reset);
    
    // Disconnect function
    socket.on('disconnect', function(data) {
        connections.splice(connections.indexOf(socket), 1);
        console.log(display.info, '[i] disconnected:', connections.length , display.info, 'sockets connected', display.reset);
    });
    
    // Login function
    socket.on('login', function(data) {
        console.log(data);
        console.log('data fields : ', data.username, " - ", data.password);
        if(data.username == "toto" && data.password == "admin") {
            socketIO.sockets.emit('login response', {response: 1}); 
        } else {
            socketIO.sockets.emit('login response', {response: 0}); 
        }
        
    });
});*/

server.listen(PORT, function() { console.log(display.important, "[+] Listening on port " + PORT, display.reset); });