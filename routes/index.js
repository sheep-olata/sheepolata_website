const express = require('express');
const router  = express.Router();
const { ensureAuthenticated, forwardAuthenticated } = require('./../config/authentication');
const display = require('./../config/colors');

// Welcome Page
router.get('/', forwardAuthenticated, (req, res) => res.render('index'));

// Dashboard
// router.get('/dashboard/:sid', ensureAuthenticated, (req, res) => {
//     res.render('dashboard', {
//       user: req.user
//     });
//   });
router.get('/dashboard', ensureAuthenticated, (req, res) =>
  res.render('dashboard', {
    user: req.user
  })
);

module.exports = router;
