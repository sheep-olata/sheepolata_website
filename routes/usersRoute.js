const express = require('express');
const router  = express.Router();
const bcrypt = require('bcryptjs');
const passport = require('passport');

const display = require('../config/colors');
const User = require('./../models/userModel');

const { forwardAuthenticated } = require('./../config/authentication');

// Login Page
router.get('/login', forwardAuthenticated, (req, res) => {
  res.render('login', {
    navabar: 'connexion'
  });

});


function getPosition(string, subString, occurrence) {
  return string.substring(string.split(subString, occurrence).join(subString).length+subString.length);
}

// Login
router.post('/login', (req, res, next) => {
  let sid = getPosition(req.headers.cookie.toString(), "sid=", 2);
  passport.authenticate('local', {
    // successRedirect: '/dashboard/'+ sid,
    successRedirect: '/dashboard',
    failureRedirect: '/login',
    failureFlash: true
  })(req, res, next);
});

// Register Page
router.get('/register', forwardAuthenticated, (req, res) => res.render('register'));


// Register
router.post('/register', (req, res) => {
    const { name, email, password, password2 } = req.body;
    let errors = [];
  
    if (!name || !email || !password || !password2) {
      errors.push({ msg: 'Please enter all fields' });
    }
  
    if (password != password2) {
      errors.push({ msg: 'Passwords do not match' });
    }
  
    if (password.length < 6) {
      errors.push({ msg: 'Password must be at least 6 characters' });
    }
  
    if (errors.length > 0) {
      res.render('register', {
        errors,
        name,
        email,
        password,
        password2
      });
    } else {
      User.findOne({ email: email }).then(user => {
        if (user) {
          errors.push({ msg: 'Email already exists' });
          res.render('register', {
            errors,
            name,
            email,
            password,
            password2
          });
        } else {
          const newUser = new User({
            name,
            email,
            password
          });
  
          bcrypt.genSalt(10, (err, salt) => {
            bcrypt.hash(newUser.password, salt, (err, hash) => {
              if (err) throw err;
              newUser.password = hash;
              newUser
                .save()
                .then(user => {
                  req.flash('success_msg', 'You are now registered and can log in');
                  res.redirect('/login');
                })
                .catch(err => console.log(err));
            });
          });
        }
      });
    }
  });

// Logout
router.get('/logout', (req, res) => {
    req.logout();
    req.flash('success_msg', 'You are logged out');
    res.redirect('/login');
});

module.exports = router;