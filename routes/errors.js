let express = require('express');
let router  = express.Router();

// Get Error 404 (Need to be called at the end)
router.get('*', function(request, response) {
        response.status(404);
        response.set({
                'Desc': "La page demandée n'existe pas",
                'Content-Type': 'text/html'});
        response.render('error-404');
});

module.exports = router;